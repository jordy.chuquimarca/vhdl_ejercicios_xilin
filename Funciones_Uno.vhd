----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:16:21 05/23/2022 
-- Design Name: 
-- Module Name:    Funciones_Uno - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- F0 = (A + B) * C

entity Funciones_Uno is
    Port ( A,B,C : in  STD_LOGIC;
           F0 : out  STD_LOGIC);
end Funciones_Uno;

architecture Behavioral of Funciones_Uno is

begin
F0 = (A AND B) AND C

end Behavioral;

