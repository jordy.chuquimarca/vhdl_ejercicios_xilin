----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:08:58 05/23/2022 
-- Design Name: 
-- Module Name:    Ventilador - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Ventilador is
    Port ( a,b : in  STD_LOGIC;
           e : out  STD_LOGIC);
end Ventilador;

architecture Behavioral of Ventilador is

begin
process (a,b) begin
if (a = '1') then e <= '1';
elsif (b = '1') then e <= '0';
end process; 

end Behavioral;

