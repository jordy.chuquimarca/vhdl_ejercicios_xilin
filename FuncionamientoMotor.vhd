----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:28:49 05/23/2022 
-- Design Name: 
-- Module Name:    FuncionamientoMotor - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- y = a / b + c / b + a c

entity FuncionamientoMotor is
    Port ( A,B,C : in  STD_LOGIC;
           Y : out  STD_LOGIC);
end FuncionamientoMotor;

architecture Behavioral of FuncionamientoMotor is

begin

Y <= ((A AND B) OR (C AND B) OR (A AND C));


end Behavioral;

