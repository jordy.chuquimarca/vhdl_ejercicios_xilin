----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:43:05 05/23/2022 
-- Design Name: 
-- Module Name:    VHDL_flujoDatos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity VHDL_flujoDatos is
    Port ( a0,b0,a1,b1,a2.b2,a3,b3 : in  STD_LOGIC;
           f : out  STD_LOGIC);
end VHDL_flujoDatos;

architecture Behavioral of VHDL_flujoDatos is

begin
       f <= (a0 or b0) and (a1 or b1) and
		       (a2 or b2) and (a3 or b3);


end Behavioral;

