----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:23:05 05/23/2022 
-- Design Name: 
-- Module Name:    ApagadorEscaleras - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ApagadorEscaleras is
    Port ( A,B : in  STD_LOGIC;
           C : in  STD_LOGIC);
end ApagadorEscaleras;

architecture Behavioral of ApagadorEscaleras is

begin

C <= ( A AND NOT B) OR (A AND NOT B);
end Behavioral;

