----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:48:35 05/23/2022 
-- Design Name: 
-- Module Name:    RobotFlujoDato - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RobotFlujoDato is
    Port ( S1,S2,S3,S4 : in  STD_LOGIC;
           T : out  STD_LOGIC);
end RobotFlujoDato;

architecture Behavioral of RobotFlujoDato is

begin
    T <= '1' WHEN ( S1 = '1' AND S2 = '1' AND
	                 S3 = '1' AND S4 = '1' AND) 
				 ELSE '0';

end Behavioral;

