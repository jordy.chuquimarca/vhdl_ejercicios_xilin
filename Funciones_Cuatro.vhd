----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:21:25 05/23/2022 
-- Design Name: 
-- Module Name:    Funciones_Cuatro - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- F2 = (A + C)

entity Funciones_Cuatro is
    Port ( A,B,C : in  STD_LOGIC;
           F2 : out  STD_LOGIC);
end Funciones_Cuatro;

architecture Behavioral of Funciones_Cuatro is

begin
F2 <= (A OR C);

end Behavioral;

