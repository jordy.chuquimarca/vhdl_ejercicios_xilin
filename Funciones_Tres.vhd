----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:19:53 05/23/2022 
-- Design Name: 
-- Module Name:    Funciones_Tres - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

--F = (B * C)

entity Funciones_Tres is
    Port ( A,B,C : in  STD_LOGIC;
           F : out  STD_LOGIC);
end Funciones_Tres;

architecture Behavioral of Funciones_Tres is

begin
 F <= (B AND C);

end Behavioral;

