----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:32:31 05/23/2022 
-- Design Name: 
-- Module Name:    DeclaracionFuncional - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity DeclaracionFuncional is
    Port ( a,b : in  STD_LOGIC;
           f1 : out  STD_LOGIC);
end DeclaracionFuncional;

architecture Behavioral of DeclaracionFuncional is

begin
 process (a,b) begin
 if (a = '0' and b = '0') then
     f1 <= '0';
 else
     f1 <= '1';
end if;
end process;

end Behavioral;

