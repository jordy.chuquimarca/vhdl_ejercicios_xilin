----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:31:18 05/23/2022 
-- Design Name: 
-- Module Name:    ProyectoNueveDos - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ProyectoNueveDos is
    Port ( P0,P1,P2 : in  STD_LOGIC;
           X : BUFFER  STD_LOGIC;
           A0,A1 : out  STD_LOGIC);
end ProyectoNueveDos;

architecture Behavioral of ProyectoNueveDos is
begin
end Behavioral;

