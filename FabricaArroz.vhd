----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:12:23 05/23/2022 
-- Design Name: 
-- Module Name:    FabricaArroz - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FabricaArroz is
    Port ( a,b : in  STD_LOGIC;
           c,d,e : out  STD_LOGIC);
end FabricaArroz;

architecture Behavioral of FabricaArroz is

begin
c <= '1' when (a = '0' and b = '0') 
     else 
      d <= '1' and e <= '1' when (a = '1' and b = '1');

end Behavioral;

