----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:45:33 05/23/2022 
-- Design Name: 
-- Module Name:    FlujoDatosVectores - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FlujoDatosVectores is
    Port ( a,b : in  STD_LOGIC_VEctor(0 to 3);
           f : out  STD_LOGIC);
end FlujoDatosVectores;

architecture Behavioral of FlujoDatosVectores is

begin
     f <= (a(0) or b(0)) and (a(1) or b(1)) and
	       (a(2) or b(2)) and (a(3) or b(3));
 
end Behavioral;

